package com.example.tarik.firstrealprojectfromskiff;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * Created by tarik on 16.01.17.
 */

public class ManagerCongratulation extends AppCompatActivity{

    TextView areaForCongratulation;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manager_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        areaForCongratulation = (TextView) findViewById(R.id.areaForCongratulation);

        String k = getIntent().getStringExtra("someCongratulation");
        areaForCongratulation.setText(k);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case android.R.id.home:
                onBackPressed();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        SettingsActivity.this.overridePendingTransition(R.anim.trans_up_out, R.anim.trans_down_in);
    }
}