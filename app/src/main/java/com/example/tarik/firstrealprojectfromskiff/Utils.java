package com.example.tarik.firstrealprojectfromskiff;


import android.os.Handler;
import android.view.View;

public class Utils {

    public static void disableDoubleClick(final View v) {
        v.setClickable(false);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                v.setClickable(true);
            }
        }, 1500);
    }


}
