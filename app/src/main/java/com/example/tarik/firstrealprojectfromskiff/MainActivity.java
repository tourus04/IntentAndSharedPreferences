package com.example.tarik.firstrealprojectfromskiff;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener, AdapterView.OnItemSelectedListener {

    EditText editText1;
    EditText editText2;
    EditText editText3;
    Button loadId;
    Button saveId;
    SeekBar seekBar;
    /*Switch simpleSwitch;
    String statusSwitch;*/

    public String lastCheckedSpinner;
    public String mOwner;

    String[] profNames = {"Developer", "Cleaner", "Manager"};


    public SharedPreferences sharedPreferences1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowTitleEnabled(false);

        editText1 = (EditText) findViewById(R.id.editText1);
        editText2 = (EditText) findViewById(R.id.editText2);
        editText3 = (EditText) findViewById(R.id.editText3);
        loadId = (Button) findViewById(R.id.loadId);
        saveId = (Button) findViewById(R.id.saveId);
        seekBar = (SeekBar) findViewById(R.id.seekBar);



       // simpleSwitch = (Switch) findViewById(R.id.simpleSwitch);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, profNames);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        Spinner spinner = (Spinner) findViewById(R.id.simpleSpinner);
        spinner.setAdapter(adapter);







        sharedPreferences1 = getSharedPreferences(Settings.FILE_NAME, Context.MODE_PRIVATE);

        spinner.setOnItemSelectedListener(this);
        loadId.setOnClickListener(this);
        saveId.setOnClickListener(this);

        /*seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

        }*/

    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.loadId:
                if (!editText1.getText().toString().equals("")) {
                    Settings.setFirstName(editText1.getText().toString(), sharedPreferences1);
                } else {
                    Toast.makeText(MainActivity.this, "Заполните все поля ввода!", Toast.LENGTH_SHORT).show();
                    return;
                }
                Intent mIntent = new Intent(MainActivity.this, YourProfile.class);
                mIntent.putExtra ("PASS_YOUR_LASTNAME", editText2.getText().toString());
                mIntent.putExtra ("PASS_YOUR_POSITION", editText3.getText().toString());
                startActivity(mIntent);
                break;



            // mIntent.putExtra("switch", simpleSwitch.isChecked());


            //Settings.setSwitchButton(simpleSwitch.isChecked(), sharedPreferences1);

            /*switch (saveId.getId()) {
                case R.id.saveId:
                    if (profNames[position].equals("Developer")) {

                    }*/

            case R.id.saveId:
                mOwner = lastCheckedSpinner;

        }



    }

    @Override
    public void onItemSelected(AdapterView<?> arg0, View arg1, int position, long id) {
        //Toast.makeText(getApplicationContext(), profNames[position], Toast.LENGTH_LONG).show();
        editText3.setText(profNames[position]);

        lastCheckedSpinner = profNames[position];


        if (profNames[position].equals(mOwner) ) {  //Manager
            Intent mIntent = new Intent(MainActivity.this, ManagerCongratulation.class);
            mIntent.putExtra("someCongratulation", profNames[position]);
            startActivity(mIntent);



        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> arg0) {

    }


}
