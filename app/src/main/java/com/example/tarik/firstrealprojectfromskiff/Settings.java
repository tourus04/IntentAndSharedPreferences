package com.example.tarik.firstrealprojectfromskiff;


import android.content.SharedPreferences;

public class Settings {

    public static final String FILE_NAME = "file_name";
    public static final String INPUT_TEXT = "input_text";

    public static final String FIRST_NAME = "your_first_name";
   /* public static final String LAST_NAME = "your_last_name";
    public static final String POSITION = "your_position";*/
    public static final String SWTCH_BUTTON = "switch_button";





    public static void setFirstName(String onePhoto, SharedPreferences sharedPreferences) {
        SharedPreferences.Editor mEditor = sharedPreferences.edit();
        mEditor.putString(String.valueOf(FIRST_NAME), onePhoto);
        mEditor.apply();
    }

    public static String getFirstName(SharedPreferences sharedPreferences) {
        return sharedPreferences.getString(FIRST_NAME, "");
    }

///////////////////////////////////////////////////////////////////////////////////////////////////


    public static void setSwitchButton(Boolean firstMethod, SharedPreferences sharedPreferences) {
        SharedPreferences.Editor mEditor = sharedPreferences.edit();
        mEditor.putBoolean(SWTCH_BUTTON, firstMethod);
        mEditor.apply();
    }


    public static boolean getSwitchButton(SharedPreferences sharedPreferences) {
        return sharedPreferences.getBoolean(SWTCH_BUTTON, true);
    }


 //////////////////////////////////////////////////////////////////////////////////////////////////


   /* public static void setLastName(String onePhoto, SharedPreferences sharedPreferences) {
        SharedPreferences.Editor mEditor = sharedPreferences.edit();
        mEditor.putString(String.valueOf(LAST_NAME), onePhoto);
        mEditor.apply();
    }



    public static String getLastName(SharedPreferences sharedPreferences) {
        return sharedPreferences.getString(LAST_NAME, "Введите вашу фамилию!");
    }



///////////////////////////////////////////////////////////////////////////////////////////////////



    public static void setPosition(String onePhoto, SharedPreferences sharedPreferences) {
        SharedPreferences.Editor mEditor = sharedPreferences.edit();
        mEditor.putString(String.valueOf(POSITION), onePhoto);
        mEditor.apply();
    }



    public static String getPosition(SharedPreferences sharedPreferences) {
        return sharedPreferences.getString(POSITION, "Введите вашу должность!");
    }

*/



}
