package com.example.tarik.firstrealprojectfromskiff;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;


/**
 * Created by tarik on 12.01.17.
 */

public class YourProfile extends AppCompatActivity {


    TextView textView2;


    public SharedPreferences sharedPreferences2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.save_profile);



        textView2 = (TextView) findViewById(R.id.textView2);


          getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        sharedPreferences2 = getSharedPreferences(Settings.FILE_NAME, Context.MODE_PRIVATE);
        /*Log.e("####", "onCreate: "+Settings.getFirstName(sharedPreferences2));
        Log.e("####", "onCreate: "+textView1);*/



        //textView1.setText("Ваше имя: " + Settings.getFirstName(sharedPreferences2));

        String lastNN = getIntent().getStringExtra("PASS_YOUR_LASTNAME");
        // textView2.setText("Ваша фамилия: " + s);

        String profession = getIntent().getStringExtra("PASS_YOUR_POSITION");


        // textView3.setText("Ваша позиция: " + k);

        textView2.setText("Ваше имя: " + Settings.getFirstName(sharedPreferences2) + "\n" + "Ваша фамилия: " + lastNN + "\n" + "Ваша позиция: " + profession);
       /* boolean btn = Settings.getSwitchButton(sharedPreferences2);
        if (btn) {
            textView2.setTextColor(Color.GREEN);
        } else {
            textView2.setTextColor(Color.RED);
        }*/



        if (profession.equals("Developer")) {
            textView2.setTextColor(Color.GREEN);
        } else if (profession.equals("Cleaner")) {
            textView2.setTextColor(Color.RED);
        }


/*
        boolean btn = getIntent().getBooleanExtra("switch", true);
        if (btn) {
            textView2.setTextColor(Color.GREEN);
        } else {
            textView2.setTextColor(Color.RED);
        }*/




//      Toast.makeText(this, Settings.getFirstName(sharedPreferences2), Toast.LENGTH_LONG).show(); //вместо тоста нужно установить текст вью
        // что бы текст куда то принялся и остался.


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

          switch (item.getItemId()){

              case android.R.id.home:
                  onBackPressed();
                  break;

          }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
//        SettingsActivity.this.overridePendingTransition(R.anim.trans_up_out, R.anim.trans_down_in);
    }
}

